package uk.ac.bham.cs.nlp;

import com.saplo.api.client.SaploClient;
import com.saplo.api.client.SaploClientException;
import com.saplo.api.client.entity.SaploCollection;
import com.saplo.api.client.entity.SaploTag;
import com.saplo.api.client.entity.SaploText;
import com.saplo.api.client.manager.SaploCollectionManager;
import com.saplo.api.client.manager.SaploTextManager;
import org.apache.log4j.BasicConfigurator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by elliotcranmer on 28/06/2016.
 */

//api key e7cfbb67da1df44f50ebb046f865e304
    // secret key fa35b8871bfd943aaff727c7f0df530a

/**
 * This class queries the SAPLO NER service
 *
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class SaploEntity {


    /**
     *  * * This method queries the services and extracts the required results ready for aggregation
     * @return the results in a map
     * @param text is the text to be analysed
     * @throws SaploClientException
     */
    public static Map<String, String> getEntities(String text) throws SaploClientException {

        BasicConfigurator.configure();
        // Connect to the SaploEntity-API
        SaploClient client = new SaploClient.Builder("e7cfbb67da1df44f50ebb046f865e304","fa35b8871bfd943aaff727c7f0df530a").build();

        // Get a manager to handle Text
        SaploTextManager textMgr = new SaploTextManager(client);


        // Create a SaploCollectionManager to work with collections
        SaploCollectionManager collectionMgr = new SaploCollectionManager(client);

        //delete old saplo queries
        for (SaploCollection sc : collectionMgr.list()) {
            collectionMgr.delete(sc.getId());
        }

        // Create a Collection object
        SaploCollection collection = new SaploCollection("My collection", SaploCollection.Language.en);

        // Save/create your collection on the SaploEntity-API
        collectionMgr.create(collection);

        // Create and save new Text
        SaploText myText = new SaploText();
        myText.setCollection(collection);
        myText.setBody(text);
        textMgr.create(myText);


        List<SaploTag> tags = textMgr.tags(myText);

        Map<String, String> results = new HashMap();


        for(SaploTag tag : tags) {

            results.put(tag.getTagWord(), tag.getCategory().toString());


        }
        return results;
    }


}
