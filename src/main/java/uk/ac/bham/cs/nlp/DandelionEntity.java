package uk.ac.bham.cs.nlp;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.BasicConfigurator;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class queries the DandelionEntity NER service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class DandelionEntity {

    private String text;
    private Map<String, String> results = new HashMap<String, String>();

    public DandelionEntity(String text) {
        this.text = text;
    }


    /**
     * This method queries the services and extracts the required results ready for aggregation
     *
     * @return the results in a map
     * @throws IOException
     * @throws ParseException
     */
    public Map<String, String> getEntities() throws IOException, ParseException {

        //return results;

        Map<String, String> entities;
        try {
            BasicConfigurator.configure();
            //Set the client
            HttpClient client = new HttpClient();

            client.getParams().setParameter("http.useragent", "Dandelion Rest Client");
            //Tell the client where to post

            PostMethod method = new PostMethod("https://api.dandelion.eu/datatxt/nex/v1");

            method.addParameter("text", text);
            method.addParameter("token", "c7f943ecabc24083a7a75849dcbbf0ac");
            method.addParameter("include", "types");

            int returnCode = client.executeMethod(method);

            String jsonAsString = method.getResponseBodyAsString();

            entities = new HashMap();


            boolean decider;

            //parse the json string back to a JsonElement then to as JsonObject
            JsonElement jelement = new JsonParser().parse(jsonAsString);
            JsonObject jobject = jelement.getAsJsonObject();

            //get the JsonArray for annotations
            JsonArray jarray = jobject.getAsJsonArray("annotations");

            //for each annotation we need to get the entity (spot) and the classes (types)
            for (int i = 0; i < jarray.size(); i++) {
                //if boolean remains true the entity will be added as it has a type
                decider = true;
                jobject = jarray.get(i).getAsJsonObject();
                JsonArray jarray2 = jobject.getAsJsonArray("types");
                String result = jobject.get("spot").toString();
                result = result.replaceAll("\"", "");

                String rezza = null;


                try {
                    rezza = jarray2.get(jarray2.size() - 1).toString();

                    //for the class recognised, just get the last word which is the class.

                    String[] bits = rezza.split("/");
                    rezza = bits[bits.length - 1];
                    rezza = rezza.replaceAll("\"", "");

                    //System.out.println(jarray2.get(jarray2.size() - 1).toString());
                } catch (ArrayIndexOutOfBoundsException e) {
                    //System.out.println("no classes");
                    decider = false;
                }

                //if decider is false then dandelion did not recognise a type for the entity, so it was not added.
                if (decider == true) {
                    entities.put(result, rezza);
                }
                //System.out.println(entities.toString());


            }

            return entities;

        } catch (Exception e) {

            entities = new HashMap();
            return entities;
        }

    }


}
