package uk.ac.bham.cs.nlp;

import com.saplo.api.client.SaploClientException;
import org.apache.commons.lang3.tuple.Pair;
import org.json.simple.parser.ParseException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * This Class is the GUI for the whole project
 *
 * @author Elliot Cranmer
 * @Date 14/08/2016.
 */
public class LisaGUI extends JFrame {

    private JPanel testPanel;
    private JPanel north;
    private JPanel south;
    private JPanel enterPanel;
    private JTextField textField;
    private JButton analyseButton;
    private JTextArea sentencetextArea2;
    private JLabel sentencLabel;
    private JLabel enterLabel;
    private JTextArea sentimenttextArea;
    private JLabel sentimentLabel;
    private JPanel southWestPanel;
    private JTextArea EntityText;
    private JLabel entityLabel;
    private JLabel classField;
    private JTextArea classTextArea;
    private JTextArea disjointTextArea;

    /**
     * This method sets an action listener on the button, which subsequently fills the text areas once processing is complete
     */
    public LisaGUI() {
        super("hello");

        analyseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {


                sentencetextArea2.setText("");
                sentimenttextArea.setText("");
                EntityText.setText("");
                classTextArea.setText("");


                Map<String, String> res;
                Map<String, Pair<String, Set<String>>> res2;
                int i = 0;
                int j = 0;
                try {
                    res = LISAMain.getSentiment(textField.getText());
                    res2 = LISAMain.getEntities(textField.getText());


                    for (Map.Entry<String, String> x : res.entrySet()) {
                        i++;

                        sentencetextArea2.append(i + ". " + x.getKey() + "\n");
                        sentimenttextArea.append(i + ". " + x.getValue() + "\n");
                    }


                    for (Map.Entry<String, Pair<String, Set<String>>> x : res2.entrySet()) {
                        j++;
                        EntityText.append(j + ". " + x.getKey() + "\n");
                        classTextArea.append(j + ". Chosen Class: " + x.getValue().getLeft());

                        if (x.getValue().getRight().isEmpty() == false) {
                            classTextArea.append(".  Removed Classes: " + x.getValue().getRight() + "\n");
                        } else {
                            classTextArea.append("\n");
                        }

                    }

                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (OWLOntologyCreationException e1) {
                    e1.printStackTrace();
                } catch (ParseException e1) {
                    e1.printStackTrace();
                } catch (SaploClientException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    /**
     * The main method initialises the frame
     *
     * @param args no arguments are entered in here
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("LisaGUI");
        frame.setContentPane(new LisaGUI().testPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
