package uk.ac.bham.cs.nlp;

/**
 * Created by elliotcranmer on 08/07/2016.
 */
public enum Service {
    ALCHEMY_API, SAPLO, THEY_SAY, MASHAPE, LISA, DANDELION, OPENCALAIS
}
