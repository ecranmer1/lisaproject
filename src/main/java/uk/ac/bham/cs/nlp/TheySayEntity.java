package uk.ac.bham.cs.nlp;

import io.theysay.affectr.client.AffectR;
import io.theysay.affectr.client.api.NamedEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * This class queries the TheySaySentiment NER service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class TheySayEntity {

    /**
     * This method queries the services and extracts the required results ready for aggregation
     *
     * @param text is the text to be analysed
     * @return the results in a map
     */
    public static Map<String, String> getEntities(String text) {
        //initialise the HashMap containing all the recognised entities and their corresponding catagories.
        Map<String, String> theEnts = new HashMap<String, String>();

        AffectR.accountDetails.setUsername("exc535@student.bham.ac.uk");
        AffectR.accountDetails.setPassword("ohj1aijaiCho");

        //an array of all the NamedEntities which contain entities, catagories etc
        NamedEntity[] Entitys = AffectR.api.getNamedEntities(text);

        // loop through all the namedEntities and add the head to the key of the map and the catagories to the values
        for (NamedEntity ents : Entitys) {

            //System.out.println(ents.getHead());

            String results = ents.getNamedEntityTypes()[0];


            String[] resultParts = results.split("\\.");
            String last = resultParts[resultParts.length-1];



            // add to the entity and the subclass to the map
            theEnts.put(ents.getHead(), last);

            //System.out.println(ents.getNamedEntityTypes()[0]);
        }
        return theEnts;
    }


}
