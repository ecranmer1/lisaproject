package uk.ac.bham.cs.nlp;

import com.saplo.api.client.SaploClientException;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.log4j.PropertyConfigurator;
import org.json.simple.parser.ParseException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

/**
 * This class is the center class of the program, it enables the GUI to call the methods form both parts, namely NER and SA.
 * @author elliot cranmer
 * @date 10/08/2016.
 */
public class LISAMain {

    private static Logger logger = LoggerFactory.getLogger(EntityMain.class);

    /**
     * This method calls the sentiment analysis part of the program
     */
    public static Map<String, String> getSentiment(String text) throws IOException {

        return OpinionMain.enterText(text);

    }

    /**
     * This method calls the NER part of the program
     */
    public static Map<String, Pair<String, Set<String>>> getEntities(String text) throws IOException, OWLOntologyCreationException, ParseException, SaploClientException {

        PropertyConfigurator.configure("config/log4j.properties");
        Map<String, Map<Service, String>> hi = EntityMain.makeQueries(text);

        OWLFinal.InitialiseProtege();
        logger.info("Analysis terminated.");

        return OWLFinal.eachEntity(hi);


    }



}
