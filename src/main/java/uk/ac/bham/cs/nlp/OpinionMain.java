package uk.ac.bham.cs.nlp;

//import org.apache.commons.codec.binary.StringUtils;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static uk.ac.bham.cs.nlp.Service.*;

/**
 * This is the main class for sentiment analysis. It calls all individual classes that query the online services.
 * The individual results are combined into a map and then aggregated.
 * @author elliotcranmer
 * @date 02/07/2016.
 */
public class OpinionMain {

    //
    private static List<Pair<Service, Double>> res;
    private static Map<String, List<Pair<Service, Double>>> finalResults = new LinkedHashMap<String, List<Pair<Service, Double>>>();

    /**
     This method where the user enters text to be analysed. It splits the text into sentences
     */
    public static Map<String, String> enterText(String text) throws IOException {


        //splits the text into individual sentences
        String[] sentences = StringUtils.split(text, ".\\\\?\\\\!");

        return makeQueries(sentences);



    }

    /**
     * This method queries all the services for all the sentences found in the text
     * @param sents is an array of sentences from the text imputed
     */
    public static Map<String, String> makeQueries(String[] sents) throws IOException {


        finalResults = new LinkedHashMap<String, List<Pair<Service, Double>>>();

        // for every sentence in the text
        for (String x : sents) {

            // create a new arraylist with the service and the result as the pair.
            res = new ArrayList<Pair<Service, Double>>();
            //a list of all the double results from all the services
            List<Double> rezas = new ArrayList<Double>();


            //create an instance of class AlchemyApi and start it on a thread
            AlchemySentiment a = new AlchemySentiment(x);
            Thread t1 = new Thread(a);
            t1.start();

            double resultA;

            int count = 0;

            //while the result is not within the correct range, try again for three seconds
            while (a.giveResult() > 10.0 || a.giveResult() < -10.0) {

                count++;

                if (count == 300) {
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            //if the result is within the given range then add it to
            if (a.giveResult() <= 10.0 && a.giveResult() >= -10.0) {
                resultA = a.giveResult();
                Pair<Service, Double> Alch = new ImmutablePair<Service, Double>(ALCHEMY_API, resultA);
                rezas.add(resultA);
                res.add(Alch);
            } else {
                Pair<Service, Double> Alch = new ImmutablePair<Service, Double>(ALCHEMY_API, 100.0);
                res.add(Alch);
            }


            TheySaySentiment t = new TheySaySentiment(x);
            Thread t2 = new Thread(t);
            t2.start();

            count = 0;

            while (t.giveResult() > 10.0 || t.giveResult() < -10.0) {

                count++;

                if (count == 299) {
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            if (t.giveResult() <= 10.0 && t.giveResult() >= -10.0) {
                double resultB = t.giveResult();
                Pair<Service, Double> They = new ImmutablePair<Service, Double>(THEY_SAY, resultB);
                rezas.add(resultB);
                res.add(They);
            } else {
                Pair<Service, Double> They = new ImmutablePair<Service, Double>(THEY_SAY, 100.0);
                res.add(They);
            }

            ViveknSentiment m = new ViveknSentiment(x);
            Thread t3 = new Thread(m);
            t3.start();

            count = 0;

            while (m.giveResult() > 10.0 || m.giveResult() < -10.0) {

                count++;

                if (count == 297) {
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            if (m.giveResult() <= 10.0 && m.giveResult() >= -10.0) {
                double resultC = m.giveResult();
                Pair<Service, Double> Mash = new ImmutablePair<Service, Double>(MASHAPE, resultC);
                rezas.add(resultC);
                res.add(Mash);
            } else {
                Pair<Service, Double> Mash = new ImmutablePair<Service, Double>(MASHAPE, 100.0);
                res.add(Mash);
            }


            DandelionSentiment d = new DandelionSentiment(x);
            Thread t4 = new Thread(d);
            t4.start();


            count = 0;

            while (d.giveResult() > 10.0 || d.giveResult() < -10.0) {

                count++;

                if (count == 298) {
                    break;
                }

                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


            if (d.giveResult() <= 10.0 && d.giveResult() >= -10.0) {

                double resultD = d.giveResult();
                Pair<Service, Double> Dand = new ImmutablePair<Service, Double>(DANDELION, resultD);
                rezas.add(resultD);
                res.add(Dand);
            } else {
                Pair<Service, Double> Dand = new ImmutablePair<Service, Double>(DANDELION, 100.0);
                res.add(Dand);
            }



            // send the results to the method
            Double lisa = aggregateResults(rezas);

            //add it into a pair with the name
            Pair<Service, Double> lisaResult = new ImmutablePair<Service, Double>(Service.LISA, lisa);

            //each pair is then added to the arraylist

            res.add(lisaResult);

            // the array list is added to the final map of results along with the corresponding sentence
            finalResults.put(x, res);
        }


        return giveAnswer(finalResults);

    }


    /**
     * This method rounds every score in the map and its corresponding score from LISA
     * @param last
     * @return a map with the chosen double and all other doubles
     */
    public static Map<String, String> giveAnswer(Map<String, List<Pair<Service, Double>>> last) {
        //gets all the results from the map, ie the sentences, the ratings and where the rating comes from

        Map<String, String> forGui = new LinkedHashMap<String, String>();

        for (Map.Entry<String, List<Pair<Service, Double>>> e : last.entrySet()) {
            //each sentence
            String key = e.getKey();
            List<Pair<Service, Double>> values = e.getValue();
            //get(3) returns the aggregated score from LISA, as the first 3 are from the original aggregators

            double lastLISA = Math.round(values.get(4).getRight() * 100.0) / 100.0;
            double Alchy = Math.round(values.get(0).getRight() * 100.0) / 100.0;
            double TheyS = Math.round(values.get(1).getRight() * 100.0) / 100.0;
            double Mashy = Math.round(values.get(2).getRight() * 100.0) / 100.0;
            double Dandy = Math.round(values.get(3).getRight() * 100.0) / 100.0;
            //could add all these two an arraylist of strings and them print it out.
            System.out.println("Sentence: " + key + ". . . Sentence sentiment LISA = " + lastLISA + ". AlchemySentiment = " + Alchy + ". TheySaySentiment = " + TheyS + ". ViveknSentiment = " + Mashy + " Dandelion = " + Dandy);


            String val = "Chosen Sentiment = " + lastLISA + ".  AlchemySentiment = " + Alchy + ". TheySaySentiment = " + TheyS + ". ViveknSentiment = " + Mashy + " Dandelion = " + Dandy;
            forGui.put(key, val);
        }

        return forGui;

    }

    /**
     * This method takes a list of the three double results from the four online sentiment analysis tools and aggregates them
     * @param rezas is the list
     * @return a single double score representing LISA's aggregated score
     */
    public static Double aggregateResults(List<Double> rezas) {



        //currently just takes an average of the four tools
        int neuts = 0;
        int negs = 0;
        int pos = 0;

        List<Double> positive = new ArrayList<Double>();
        List<Double> neutral = new ArrayList<Double>();
        List<Double> negative = new ArrayList<Double>();

        double total = 0.0;

        for (double y : rezas) {
            if (y == 0) {
                neuts++;
                neutral.add(y);
            }
            if (y < 0) {
                negs++;
                negative.add(y);
            }
            if (y > 0) {
                pos++;
                positive.add(y);
            }
        }

        if (pos > negs && pos >= neuts) {

            for (double x : positive) {
                total += x;
            }
            total = total / positive.size();

        }

        if (negs > pos && negs >= neuts) {

            for (double x : negative) {
                total += x;
            }
            total = total / negative.size();
        }

        if (pos == 2 && negs == 2) {

            double n = 0.0;
            double p = 0.0;

            for (double x : positive) {
                p += x;
            }
            for (double y : negative) {
                n += y;
            }

            double n2 = Math.abs(n);
            if (n2 > p) {
                total = n / 2;
            } else total = p / 2;

        }

        if (neuts > pos && neuts > negs) {
            total = 0.0;
        }


        return total;


    }


}
