package uk.ac.bham.cs.nlp;


import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.log4j.BasicConfigurator;


/**
 * This class queries the Vivekn SA service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class ViveknSentiment implements Runnable {

    private double returnValue = -100.0;
    private String parameter;

    /**
     * This sets the constructor
     *
     * @param result is the sentence to be analysed
     */
    public ViveknSentiment(String result) {
        this.parameter = result;
    }


    /**
     * When the thread starts it queries the service
     */
    public void run() {

        try {
            BasicConfigurator.configure();

            HttpResponse<JsonNode> response = null;
            try {
                response = Unirest.post("https://community-sentiment.p.mashape.com/text/")
                        .header("X-ViveknSentiment-Key", "NDSGcRBMwImshs21Vc4gQ0lBnAQQp1QF78zjsnYAPwE2R8YR1N")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .header("Accept", "application/json")
                        .field("txt", parameter)
                        .asJson();
            } catch (UnirestException e) {
                e.printStackTrace();
            }

            String theAnswer = String.valueOf(response.getBody());
            String[] parts = theAnswer.split("\"");

            String theResult = parts[5];
            String theconfidence = parts[9];
            Double confidence = Double.parseDouble(theconfidence);


            if (theResult.equals("Positive")) {
                double answer = confidence / 10.0;
                returnValue = answer;

            }
            if (theResult.equals("Negative")) {
                double answer = -confidence / 10.0;
                returnValue = answer;

            }
            if (theResult.equals("Neutral")) {
                //Double answer / 10.0;
                returnValue = 0.0;

            }
        } catch (Exception e) {
            System.err.println("Error with ViveknSentiment. Wait for thread to close");
        }

    }


    /**
     * returns the sentiment of the sentence
     *
     * @return the result as a double
     */
    public double giveResult() {

        return returnValue;

    }


}
