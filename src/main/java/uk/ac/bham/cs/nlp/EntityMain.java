package uk.ac.bham.cs.nlp;

import com.saplo.api.client.SaploClientException;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;




/**
 * This is the main class for the NER services. It calls all individual classes that query the online services.
 * It adds all results from the individual services into a map and sends to OWLFinal to be aggregated.
 * @author elliot Cranmer
 * @date 17/07/2016.
 */
public class EntityMain {


    private static Map<String, Map<Service, String>> finalResults;
    private static Logger logger = LoggerFactory.getLogger(EntityMain.class);


    /**
     * This method calls each of the individual classes that query the online service. It then combines there results into a map to be sent for aggregation
     *
     * @param text is the text entered by the user to be analysed
     * @return a map with each entity and the services and classes associated with it
     * @throws SaploClientException
     * @throws IOException
     * @throws ParseException
     */
    public static Map<String, Map<Service, String>> makeQueries(String text) throws SaploClientException, IOException, ParseException {


        //gets a map of all entities and the classes they belong too

        Map<String, String> theySay;
        Map<String, String> Alch;
        Map<String, String> Sapl;
        Map<String, String> Calais;
        Map<String, String> Dand;

        try {
            theySay = TheySayEntity.getEntities(text);
        } catch (Exception e) {
            theySay = new HashMap<String, String>();
        }

        try {
            Alch = AlchemyEntity.getEntities(text);
        } catch (Exception e) {
            Alch = new HashMap();
        }
        try {
            Sapl = SaploEntity.getEntities(text);
        } catch (Exception e) {
            Sapl = new HashMap();
        }

        try {
            Calais = OpenCalaisEntity.getEntities(text);
        } catch (Exception e) {
            Calais = new HashMap();
        }

        try {
            DandelionEntity d = new DandelionEntity(text);
            Dand = d.getEntities();
        } catch (Exception e) {
            Dand = new HashMap();
        }




        //initialised the data structure to store all the entities and classes
        finalResults = new LinkedHashMap<String, Map<Service, String>>();

        //creates a tempory storage map for the the service and the classes. this will be the value for each entity
        Map<Service, String> tempMap;

        //loops thorough all entries in each map starting with THEYSAY and adds all entities, service name and classes to the final results map.
        for (Map.Entry<String, String> entry : theySay.entrySet()) {
            tempMap = new LinkedHashMap<Service, String>();

            String eachEntity = entry.getKey();
            String possibleRes = entry.getValue();

            tempMap.put(Service.THEY_SAY, possibleRes);
            finalResults.put(eachEntity, tempMap);
        }

        //fOR Every entity recognised in Alchemy.
        for (Map.Entry<String, String> entry : Alch.entrySet()) {

            tempMap = new LinkedHashMap<Service, String>();

            String eachEntity = entry.getKey();

            // if the key (the entity) is already in finalresults
            if (finalResults.containsKey(eachEntity)) {
                tempMap = finalResults.get(eachEntity);

                String possibleRes = entry.getValue();

                tempMap.put(Service.ALCHEMY_API, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }
            //if the key is not already in final results.
            else {

                String possibleRes = entry.getValue();

                tempMap.put(Service.ALCHEMY_API, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }
        }

        //for every entity in SaploEntity
        for (Map.Entry<String, String> entry : Sapl.entrySet()) {

            tempMap = new LinkedHashMap<Service, String>();

            String eachEntity = entry.getKey();

            // if the key (the entity) is already in finalresults
            if (finalResults.containsKey(eachEntity)) {
                tempMap = finalResults.get(eachEntity);

                String possibleRes = entry.getValue();

                tempMap.put(Service.SAPLO, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }
            //if the key is not already in final results.
            else {

                String possibleRes = entry.getValue();

                tempMap.put(Service.SAPLO, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }

        }

        //for every entity in Open Calais
        for (Map.Entry<String, String> entry : Calais.entrySet()) {

            tempMap = new LinkedHashMap<Service, String>();

            String eachEntity = entry.getKey();

            // if the key (the entity) is already in finalresults
            if (finalResults.containsKey(eachEntity)) {
                tempMap = finalResults.get(eachEntity);

                String possibleRes = entry.getValue();

                tempMap.put(Service.OPENCALAIS, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }
            //if the key is not already in final results.
            else {

                String possibleRes = entry.getValue();

                tempMap.put(Service.OPENCALAIS, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }

        }

        //for every entity in Dandelion
        for (Map.Entry<String, String> entry : Dand.entrySet()) {

            tempMap = new LinkedHashMap<Service, String>();

            String eachEntity = entry.getKey();

            // if the key (the entity) is already in finalresults
            if (finalResults.containsKey(eachEntity)) {
                tempMap = finalResults.get(eachEntity);

                String possibleRes = entry.getValue();

                tempMap.put(Service.DANDELION, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }
            //if the key is not already in final results.
            else {

                String possibleRes = entry.getValue();

                tempMap.put(Service.DANDELION, possibleRes);
                finalResults.put(eachEntity, tempMap);
            }

        }

        sortEntities(finalResults);
        return finalResults;


    }

    /**
     * This method sorts the entities in the map, by dealing with any entities that are contained in other entities.
     * @param totalList is the total list of all entities and classes
     */
    public static void sortEntities(Map<String, Map<Service, String>> totalList) {

        Set<String> toBeRemoved = new HashSet();

        //for every entry in the map
        for (Map.Entry<String, Map<Service, String>> entry : totalList.entrySet()) {
            //for every key in the map
            for (Map.Entry<String, Map<Service, String>> yo : totalList.entrySet()) {
                //if the second key is contained in the first key. remove the key and add its value to the first key
                if (entry.getKey().contains(yo.getKey()) && !entry.getKey().equals(yo.getKey())) {
                    //store the deleted value in temp
                    Map<Service, String> temp = yo.getValue();
                    Map.Entry<Service, String> x = temp.entrySet().iterator().next();

                    //create a set of all the entities to be removed
                    toBeRemoved.add(yo.getKey());

                    //get the original value for each entity
                    Map<Service, String> finalTemp = entry.getValue();
                    //add the new values to it
                    finalTemp.put(x.getKey(), x.getValue());

                    //then re-add it to the final map
                    totalList.put(entry.getKey(), finalTemp);

                }
            }

        }
        //remove each entity that is redundant
        for (String x : toBeRemoved) {
            totalList.remove(x);
        }


    }


}
