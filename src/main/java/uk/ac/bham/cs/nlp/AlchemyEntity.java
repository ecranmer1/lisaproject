package uk.ac.bham.cs.nlp;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Entities;
import com.ibm.watson.developer_cloud.alchemy.v1.model.Entity;

import java.util.HashMap;
import java.util.Map;


/**
 * This class queries the AlchemySentiment NER service
 *
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class AlchemyEntity {

    /**
     * This method queries the services and extracts the required results ready for aggregation
     * @return the results in a map
     * @param text is the text to be analysed
     */
    public static Map<String, String> getEntities (String text) {

        // Get a handler for the service
        AlchemyLanguage service = new AlchemyLanguage();
        // Set the API key
        service.setApiKey("9f3f28fb4dc31c160d24b6b8776cd5fa484bec03");

        // Configure the service
        Map<String, Object> params = new HashMap<String, Object>();
        params.put(AlchemyLanguage.TEXT, text);

        Entities entities = service.getEntities(params).execute();

        Map<String, String> results = new HashMap();

        for(Entity e : entities.getEntities()) {
            results.put(e.getText(), e.getType());

        }

        return results;

    }

}
