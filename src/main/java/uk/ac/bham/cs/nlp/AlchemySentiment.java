package uk.ac.bham.cs.nlp;

import com.ibm.watson.developer_cloud.alchemy.v1.AlchemyLanguage;
import com.ibm.watson.developer_cloud.alchemy.v1.model.DocumentSentiment;

import java.util.HashMap;
import java.util.Map;

/**
 * This class queries the AlchemySentiment SA service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class AlchemySentiment implements Runnable {

    private String parameter;
    private double returnValue = -100.0;

    /**
     * This sets the constructor
     *
     * @param result is the sentence to be analysed
     */
    public AlchemySentiment(String result) {

        this.parameter = result;
    }

    /**
     * When the thread starts it queries the service
     */
    public void run() {


        try {
            // Get a handler for the service
            AlchemyLanguage service = new AlchemyLanguage();
            // Set the API key
            service.setApiKey("9f3f28fb4dc31c160d24b6b8776cd5fa484bec03");

            // Configure the service
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(AlchemyLanguage.TEXT, parameter);

            DocumentSentiment sentimentResult = service.getSentiment(params).execute();

            String result = sentimentResult.getSentiment().getType().toString();

            if (result.equals("POSITIVE")) {
                double answer = sentimentResult.getSentiment().getScore() * 10.0;
                returnValue = answer;

            }
            if (result.equals("NEGATIVE")) {
                double answer = sentimentResult.getSentiment().getScore() * 10.0;
                returnValue = answer;

            }
            if (result.equals("NEUTRAL")) {
                //Double answer = sentScore2.getSentiment().getNeutral() * 10.0;
                returnValue = 0.0;

            }
        } catch (com.ibm.watson.developer_cloud.service.exception.BadRequestException e) {
            System.err.println("Error with Alchemy. Wait for thread to close");
        }
        //System.out.println("this print out is at the bottom of run. it change returnValue to = " + returnValue);

    }


    /**
     * returns the sentiment of the sentence
     *
     * @return the result as a double
     */
    public double giveResult() {

        return returnValue;


    }



}
