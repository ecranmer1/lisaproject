package uk.ac.bham.cs.nlp;

import com.clarkparsia.owlapi.explanation.PelletExplanation;
import com.clarkparsia.pellet.owlapiv3.PelletReasoner;
import com.clarkparsia.pellet.owlapiv3.PelletReasonerFactory;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.util.OWLEntityRemover;
import org.semanticweb.owlapi.util.SimpleIRIMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

/**
 * This Class handles the aggregation for the NER services. It adds all entities to the ontology, then uses the reasoner to remove disjoint classes
 * @author elliotcranmer
 * @date 28/07/2016.
 */
public class OWLFinal {


    private static OWLOntologyManager manager;
    private static File lisaFile;
    private static OWLOntology LisaOntology;
    private static OWLDataFactory dataFactory;
    private static final String _ONTO_PATH = "ontologies";
    private static final String _PROJECT_DIR = System.getProperty("user.dir");
    private static Set<String> disjointed = new HashSet<String>();
    private static Logger logger = LoggerFactory.getLogger(OWLFinal.class);

    /**
     * This method initialises the OWL ontology manager which loads all the ontologies from protege
     *
     * @throws OWLOntologyCreationException
     */
    public static void InitialiseProtege() throws OWLOntologyCreationException {

        // Getting a hold of an ontology manager.
        logger.debug("Instantiating an ontology manager");
        manager = OWLManager.createOWLOntologyManager();

        //grabs each individual protege and manually adds them to the ontology manager.
        logger.debug("loading ontologies into ontology manager...");
        manager.addIRIMapper(
                new SimpleIRIMapper( IRI.create( "http://www.cs.bham.ac.uk/lisa" ),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "FinalLisaOntology.owl").toFile())));

        manager.addIRIMapper(
                new SimpleIRIMapper( IRI.create( "http://www.cs.bham.ac.uk/lisa/alchemy" ),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "AlchemyOntology.owl").toFile())));

        manager.addIRIMapper(
                new SimpleIRIMapper( IRI.create( "http://www.cs.bham.ac.uk/lisa/saplo" ),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "SaploOntology.owl").toFile())));

        manager.addIRIMapper(
                new SimpleIRIMapper( IRI.create( "http://www.cs.bham.ac.uk/lisa/theysay" ),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "TheySayOntology.owl").toFile())));

        manager.addIRIMapper(
                new SimpleIRIMapper( IRI.create( "http://www.cs.bham.ac.uk/lisa/opencalais" ),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "OpenCalais.owl").toFile())));

        manager.addIRIMapper(
                new SimpleIRIMapper(IRI.create("http://www.cs.bham.ac.uk/lisa/dandelion"),
                        IRI.create(Paths.get(_PROJECT_DIR, _ONTO_PATH, "dbpedia_2014.owl").toFile())));


        lisaFile = Paths.get(_PROJECT_DIR, _ONTO_PATH, "FinalLisaOntology.owl").toFile();

        //sets up the OwlOntology
        LisaOntology = manager.loadOntologyFromOntologyDocument(lisaFile);
        logger.debug("done.");
    }


    /**
     * This method adds each entity to the ontology with its corresponding class and online service.
     * The method then calls the reasonerMethod for each OWL Individual
     *
     * @param resultsMap is the map containing each entity, and the class and the service corresponding
     * @return a map containing the entity, its class and all classes not chosen.
     * @throws IOException
     */
    public static Map<String, Pair<String, Set<String>>> eachEntity(Map<String, Map<Service, String>> resultsMap) throws IOException {

        OWLDataFactory dataFactory = manager.getOWLDataFactory();

        OWLClassAssertionAxiom ax = null;

        Map<String, Pair<String, Set<String>>> giveToGUIFull = new LinkedHashMap<String, Pair<String, Set<String>>>();

        for (Map.Entry<String, Map<Service, String>> entry : resultsMap.entrySet()) {

            //create an owlindividual that with the name of each entity recognised
            OWLIndividual s1 = dataFactory.getOWLNamedIndividual(IRI.create(lisaFile + "#" + entry.getKey()));

            //for each optional value for ethe entity add the indivudual to it
            for(Map.Entry<Service, String> nextEntry : entry.getValue().entrySet()) {
                //if the service is alchemy
                if (nextEntry.getKey().equals(Service.ALCHEMY_API)){

                    //create an owlclass corresponding to the class of the service and string
                    OWLClass Class1 = dataFactory.getOWLClass(IRI.create("http://www.cs.bham.ac.uk/lisa/alchemy#" + nextEntry.getValue()));

                    //create a class assertion axiom that adds the individual (the entity) to the corresponding class
                    ax = dataFactory.getOWLClassAssertionAxiom(Class1, s1);
                    //then add the class assertion axiom to the LisaOntology
                    manager.addAxiom(LisaOntology, ax);

                }
                if (nextEntry.getKey().equals(Service.THEY_SAY)){
                    //create an owlclass corresponding to the class of the service and string
                    OWLClass Class1 = dataFactory.getOWLClass(IRI.create("http://www.cs.bham.ac.uk/lisa/theysay#" + nextEntry.getValue()));

                    //create a class assertion axiom that adds the individual (the entity) to the corresponding class
                    ax = dataFactory.getOWLClassAssertionAxiom(Class1, s1);
                    //then add the class assertion axiom to the LisaOntology
                    manager.addAxiom(LisaOntology, ax);
                }
                if (nextEntry.getKey().equals(Service.SAPLO)) {
                    //create an owlclass corresponding to the class of the service and string
                    OWLClass Class1 = dataFactory.getOWLClass(IRI.create("http://www.cs.bham.ac.uk/lisa/saplo#" + nextEntry.getValue()));

                    //create a class assertion axiom that adds the individual (the entity) to the corresponding class
                    ax = dataFactory.getOWLClassAssertionAxiom(Class1, s1);
                    //then add the class assertion axiom to the LisaOntology
                    manager.addAxiom(LisaOntology, ax);
                }
                if (nextEntry.getKey().equals(Service.OPENCALAIS)) {
                    //create an owlclass corresponding to the class of the service and string
                    OWLClass Class1 = dataFactory.getOWLClass(IRI.create("http://www.cs.bham.ac.uk/lisa/opencalais#" + nextEntry.getValue()));

                    //create a class assertion axiom that adds the individual (the entity) to the corresponding class
                    ax = dataFactory.getOWLClassAssertionAxiom(Class1, s1);
                    //then add the class assertion axiom to the LisaOntology
                    manager.addAxiom(LisaOntology, ax);
                }

                if (nextEntry.getKey().equals(Service.DANDELION)) {
                    //create an owlclass corresponding to the class of the service and string
                    OWLClass Class1 = dataFactory.getOWLClass(IRI.create("http://dbpedia.org/ontology/" + nextEntry.getValue()));

                    //create a class assertion axiom that adds the individual (the entity) to the corresponding class
                    ax = dataFactory.getOWLClassAssertionAxiom(Class1, s1);
                    //then add the class assertion axiom to the LisaOntology
                    manager.addAxiom(LisaOntology, ax);
                }

            }

            /*
             * this section prints out the result for each individual, then removes that individual from the ontology before the next is added
             */

            //so here is each entity added on their own so far...

            OWLClass aClass = reasonerMethod(s1);


            for (OWLNamedIndividual HI : LisaOntology.getIndividualsInSignature()) {

                if (aClass.getIRI().getFragment().equals("OTHER")) {

                } else {
                    System.out.println("Result  =  Entity: [" + entry.getKey() + "] = Class: [" + aClass.getIRI().getFragment() + "].");
                    // + Sentiment (range -10 to 10) = + OpinionEntity.makeQueries(entry.getKey())

                    disjointed.remove(aClass.getIRI().getFragment());
                    Set<String> tempt = Sets.newHashSet(disjointed);
                    Pair<String, Set<String>> giveToGUI = new ImmutablePair<String, Set<String>>(aClass.getIRI().getFragment(), tempt);



                    giveToGUIFull.put(entry.getKey(), giveToGUI);

                }
            }


            OWLEntityRemover remover = new OWLEntityRemover(manager, Collections.singleton(LisaOntology));
            for (OWLNamedIndividual ind : LisaOntology.getIndividualsInSignature()) {
                ind.accept(remover);
            }

            manager.applyChanges(remover.getChanges());


            disjointed.clear();

        }

        return giveToGUIFull;

    }


    /**
     * This method uses the reasoner to tell whether the OWL Individual causes any inconsistencies with its class assertions.
     * It then returns the lowest subclass.
     * @param owlIndividual
     * @return
     */
    public static OWLClass reasonerMethod(OWLIndividual owlIndividual) {


        //set up reasoner
        PelletExplanation.setup();

        //add Lisa ontolgy to the reasoner
        PelletReasoner reasoner = PelletReasonerFactory.getInstance().createReasoner(LisaOntology);
        reasoner.prepareReasoner();

        //Sets up the reasoner as a pelletExplanation reasoner
        PelletExplanation expGen = new PelletExplanation(reasoner);

        reasoner.getKB().setDoExplanation(true);

        for (OWLClassExpression classes1 : owlIndividual.getTypes(LisaOntology)) {
            disjointed.add(classes1.asOWLClass().getIRI().getFragment());
        }

        /*
         * If the reasoner is consistant there are no disjoints so the method will return the class which is a subclass of the others
         */

        //this is not working at present


        if (reasoner.isConsistent()) {

            Set<OWLClassExpression> allClassExpressions = owlIndividual.getTypes(LisaOntology);


            NodeSet<OWLClass> allClasses;
            for (OWLClassExpression zzz : allClassExpressions) {

                allClasses = reasoner.getSubClasses(zzz, false);


                for (OWLClassExpression zzz2 : allClassExpressions) {
                    if (allClasses.containsEntity(zzz2.asOWLClass())) {

                        return zzz2.asOWLClass();
                    }
                }
            }


            Node<OWLClass> allOptions;
            for (OWLClassExpression ifNoSub : allClassExpressions) {
                allOptions = reasoner.getEquivalentClasses(ifNoSub);


                for (OWLClassExpression ifNoSub2 : allClassExpressions) {
                    if (allOptions.contains(ifNoSub2.asOWLClass())) {
                        if (!ifNoSub.equals(ifNoSub2)) {

                            return ifNoSub2.asOWLClass();

                        }
                    }
                }
            }

            return allClassExpressions.iterator().next().asOWLClass();

        }

        /*
         * if the reasoner is not consistent, the class with the most disjoints is deleted and the method is recalled to see if it is consistent next time
         */

        if (reasoner.isConsistent() == false) {

            //initiales the Hashset of Pairs that will store each disjointed pair of classes.
            Set<Pair<OWLClass, OWLClass>> disjoints = new HashSet<Pair<OWLClass, OWLClass>>();

            //gets a set of a set of OWLAxioms which are the inconsistency explanations
            Set<Set<OWLAxiom>> incExpl = expGen.getInconsistencyExplanations();

            // for each set in the first set
            OWLClass chosen = null;
            for (Set<OWLAxiom> explanation : incExpl) {

                // creates a tempory Arraylist of strings to store teh disjointed classes
                List<OWLClass> temp = new ArrayList<OWLClass>();
                //for each OwlAxiom in the second set
                for (OWLAxiom explAx : explanation) {


                    OWLClass z1;

                    //if the OWLAxiom is an OWLClassAssertionAxiom then it is guaranteed that those two classes are disjointed as they are going to be proven why later in the explaination
                    if (explAx instanceof OWLClassAssertionAxiom) {


                        //these classes are then stored as a string
                        z1 = explAx.getClassesInSignature().iterator().next();


                        //then added to the temp List
                        temp.add(z1);
                    }
                }



                //The temp list, each time of only two disjointed classes are added to the set of pairs...thus no duplicates are stored.
                try {
                    Pair<OWLClass, OWLClass> ans = new ImmutablePair<OWLClass, OWLClass>(temp.get(0), temp.get(1));
                    disjoints.add(ans);
                } catch (IndexOutOfBoundsException e) {
                    //don't add if not enough
                }


                //stores each class and the number of times it occurs
                Map<OWLClass, Integer> wordCount = new HashMap<OWLClass, Integer>();

                for (Pair<OWLClass, OWLClass> word : disjoints) {
                    Integer count = wordCount.get(word.getLeft());
                    wordCount.put(word.getLeft(), (count == null) ? 1 : count + 1);

                    count = wordCount.get(word.getRight());
                    wordCount.put(word.getRight(), (count == null) ? 1 : count + 1);

                }


                //attempting to find the class with the most disjoints and then store the OWLClass to make an OWL Axiom with it.
                int most = 0;

                for (Map.Entry<OWLClass, Integer> entry : wordCount.entrySet()) {

                    if (entry.getValue() > most) {
                        most = entry.getValue();
                        chosen = entry.getKey();

                    }

                }
            }


            /*
             * here i am attempting to recreate the OWLClassAssertionAxiom with the OWLclass to be deleted and the individual (owlIndividual)
             *
             * for some reason it says null pointer exception when I attempt to create it
             */


            OWLClassAssertionAxiom ax = manager.getOWLDataFactory().getOWLClassAssertionAxiom(chosen, owlIndividual);


            //then add the class assertion axiom to the LisaOntology
            manager.removeAxiom(LisaOntology, ax);


            //then after removing the OWLClassAxiom, recall the method with how ever many classes are still attached to the Individual.
            return reasonerMethod(owlIndividual);

            //return chosen;
        }
        return null;
    }


}
