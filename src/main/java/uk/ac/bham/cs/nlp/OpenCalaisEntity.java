package uk.ac.bham.cs.nlp;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.log4j.BasicConfigurator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;


/**
 * This class queries the OpenCalais NER service
 *
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class OpenCalaisEntity {

    private static final String CALAIS_URL = "https://api.thomsonreuters.com/permid/calais";
    public static String uniqueAccessKey = "o6z7nGQqGc9XVgxEDE0LQ6goMFWZ9Zvf";


    /**
     * This method queries the services and extracts the required results ready for aggregation
     * @return the results in a map
     * @param text is the text to be analysed
     * @throws IOException
     */
    public static Map<String, String> getEntities (String text) throws IOException {

        BasicConfigurator.configure();

        PostMethod method = new PostMethod(CALAIS_URL);

        // Set mandatory parameters
        method.setRequestHeader("X-AG-Access-Token", uniqueAccessKey);
        // Set input content type
        method.setRequestHeader("Content-Type", "text/raw");
        // Set response/output format
        method.setRequestHeader("outputformat", "xml/rdf");

        HttpClient client = new HttpClient();

        client.getParams().setParameter("http.useragent", "Calais Rest Client");

        method.setRequestEntity(new StringRequestEntity(text));

        int returnCode = client.executeMethod(method);

        Map<String, String> entities = new HashMap();

        if (returnCode == HttpStatus.SC_NOT_IMPLEMENTED) {
            System.err.println("The Post method is not implemented by this URI");
            // still consume the response body
            method.getResponseBodyAsStream();//here I changed from string
        } else if (returnCode == HttpStatus.SC_OK) {

            BufferedReader br = new BufferedReader(new InputStreamReader(method.getResponseBodyAsStream()));
            String readLine;
            int counter = 0;

            while(((readLine = br.readLine()) != null)) {
                counter++;

                if(readLine.startsWith("--><rdf:RDF")){
                    break;
                }
                if (counter >2){
                    String [] ents = readLine.split(": |, ");
                    for (int i = 1; i < ents.length; i++){
                        entities.put(ents[i], ents[0]);
                    }
                }
            }

        } else {
            System.err.println("File post failed: " );
            System.err.println("Got code: " + returnCode);
            System.err.println("response: "+method.getResponseBodyAsString());
        }



        return entities;



    }




}