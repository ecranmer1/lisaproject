package uk.ac.bham.cs.nlp;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.BasicConfigurator;

/**
 * This class queries the DandelionEntity SA service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class DandelionSentiment implements Runnable {

    private String parameter;
    private double returnValue = -100.0;

    /**
     * This sets the constructor
     *
     * @param result is the sentence to be analysed
     */
    public DandelionSentiment(String result) {
        this.parameter = result;

    }

    /**
     * When the thread starts it queries the service
     */
    public void run() {


        try {
            BasicConfigurator.configure();
            //Set the client
            HttpClient client = new HttpClient();

            client.getParams().setParameter("http.useragent", "Dandelion Rest Client");
            //Tell the client where to post

            PostMethod method = new PostMethod("https://api.dandelion.eu/datatxt/sent/v1");

            parameter = parameter.replaceAll("£", "");

            method.addParameter("text", parameter);
            method.addParameter("token", "c7f943ecabc24083a7a75849dcbbf0ac");
            method.addParameter("include", "types");

            client.executeMethod(method);

            String jsonAsString = method.getResponseBodyAsString();

            JsonElement jelement = new JsonParser().parse(jsonAsString);

            JsonObject jobject = jelement.getAsJsonObject();
            jobject = jobject.getAsJsonObject("sentiment");

            try {
                String catagory = jobject.get("type").toString();
                double polarity = Double.parseDouble(jobject.get("score").toString());


                if (catagory.equals("\"positive\"")) {
                    returnValue = polarity * 10;
                }
                if (catagory.equals("\"negative\"")) {
                    returnValue = polarity * 10;
                }
                if (catagory.equals("\"neutral\"")) {
                    returnValue = 0.0;
                }
            } catch (NullPointerException E) {

            }


        } catch (Exception e) {
            System.err.println("Error with Dandelion. Wait for thread to close");
        }

    }

    /**
     * returns the sentiment of the sentence
     *
     * @return the result as a double
     */
    public double giveResult() {

        return returnValue;


    }


}
