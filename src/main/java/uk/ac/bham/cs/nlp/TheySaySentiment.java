package uk.ac.bham.cs.nlp;

import io.theysay.affectr.client.AffectR;
import io.theysay.affectr.client.api.SimpleSentiment;


/**
 * This class queries the TheySaySentiment SA service
 * @author elliotcranmer
 * @date 07/07/2016.
 */
public class TheySaySentiment implements Runnable {

//Problem = the text entered must be longer than 10 chars.

    private String parameter;
    private double returnValue = -100.0;

    /**
     * This sets the constructor
     *
     * @param result is the sentence to be analysed
     */
    public TheySaySentiment(String result) {

        this.parameter = result;
    }

    /**
     * When the thread starts it queries the service
     */
    public void run() {

        try {
            AffectR.accountDetails.setUsername("exc535@student.bham.ac.uk");
            AffectR.accountDetails.setPassword("ohj1aijaiCho");

            if (parameter.length() <= 10) {
                returnValue = -100.0;
            } else {

                SimpleSentiment sentScore2 = AffectR.api.classifySentiment(parameter);

                String result = sentScore2.getSentiment().getPolarity();


                if (result.equals("POSITIVE")) {
                    double answer = sentScore2.getSentiment().getPositive() * 10.0;
                    returnValue = answer;

                }
                if (result.equals("NEGATIVE")) {
                    double answer = -sentScore2.getSentiment().getNegative() * 10.0;
                    returnValue = answer;

                }
                if (result.equals("NEUTRAL")) {
                    //Double answer = sentScore2.getSentiment().getNeutral() * 10.0;
                    returnValue = 0.0;

                }

            }
        } catch (Exception e) {
            System.err.println("Error with TheySaySentiment. Wait for thread to close");
        }
    }

    /**
     * returns the sentiment of the sentence
     *
     * @return the result as a double
     */
    public double giveResult() {

        return returnValue;


    }




}
